/*
Group #: H
Group Members: Bill Barish, Sam Sausville, Yuhan Su
Program Description: This program should imitate a basic Venmo menu system. Until the user types in a "4", the menu will continue to loop through and process transactions. Each choice (1-4) prompts the computer to complete a different function. For example, if the user types 3, the user's current balance should be printed out.

Before starting to add code, you should walk through the CT planning process. We have included multiple questions to guide the process of decomposition/algorithmic thinking.
*/

import java.util.Scanner;
public class DoWhileExample{
    public static void main(String [] args){
        Scanner input = new Scanner(System.in);
        int choice = 0; //choice will hold the users selection/input
        // input-- what variables will be needed?
        //Also need a balance variable
        int balance = 100;
        //Also need a tranaction amount variable that changes a lot
        int transaction = 0;
        //Also need a fee varibles for revenue for venmo
        final int FEE = 1;

        String trash = "";
        Scanner scan = new Scanner (System.in);




        //So far, the do-while loop simply prints out the menu options. What else needs to happen here?
        do{
            System.out.println("Venmo Main Menu");
            System.out.println("1. Send Money");
            //If the user types 1, what should the program do? Use psuedocode or english words in comments
//Should remove money from the users balance + the feee



            //If the user types 1, what is the expected output? Use psuedocode or english words in comments




            System.out.println("2. Request Money");
            //If the user types 2, what should the program do? Use psuedocode or english words in comments
//Should add money to the users balance minus the feee


            //If the user types 2, what is the expected output? Use psuedocode or english words in comments




            System.out.println("3. Check Balance");
            //If the user types 3, what should the program do? Use psuedocode or english words in comments

//Should print out the user balance


            //If the user types 3, what is the expected output? Use psuedocode or english words in comments




            System.out.println("4. Quit");
            //if the user types 4, what should the program do? Use psuedocode or english words in comments

//Should quit the program and exit the do while loop



            //If the user types 4, what is the expected output? Use psuedocode or english words in comments





            //Implement the program functionality that you defined below:


            while (!scan.hasNextInt()) {
                trash = scan.nextLine(); //Trash input
                System.out.println("Error: Please type in a valid input");
                System.out.println("How much was the next Transaction on your list");
            }//Prompts for number of transactions
            choice = scan.nextInt();
            trash = scan.nextLine();

        if (choice == 1){
            System.out.println("Tranaction Amount?");
            while (!scan.hasNextInt()) {
                trash = scan.nextLine(); //Trash input
                System.out.println("Error: Please type in a valid input");
                System.out.println("How much was the next Transaction on your list");
            }//Prompts for number of transactions
            transaction = scan.nextInt();
            trash = scan.nextLine();

            balance = balance - transaction - FEE;
            System.out.println("Amount sent: " + transaction);
            System.out.println("Fee: " + FEE);
            System.out.println("Current Balance is : " + balance);
        }else if (choice == 2){
            System.out.println("Tranaction Amount?");
            while (!scan.hasNextInt()) {
                trash = scan.nextLine(); //Trash input
                System.out.println("Error: Please type in a valid input");
                System.out.println("How much was the next Transaction on your list");
            }//Prompts for number of transactions
            transaction = scan.nextInt();
            trash = scan.nextLine();
            balance = balance + transaction;
            System.out.println("Amount Requested" + transaction);
            System.out.println("Current Balance is : " + balance);
        }else if (choice == 3){
            System.out.println("Current Balance is : " + balance);
        }else if (choice == 4){
        }else{
            System.out.println("Error: Invalid input");
        }



        }while(choice != 4);
        System.out.println("Goodbye");
    }
}
    