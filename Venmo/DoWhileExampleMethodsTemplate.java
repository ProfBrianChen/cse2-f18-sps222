import java.util.Scanner;
public class DoWhileExampleMethodsTemplate{
    public static void main(String [] args){
        Scanner input = new Scanner(System.in);
        int choice;
        double balance = 100;
        double money = 0;
        String friend;
        do{
            printMenu();
            choice = getInt(input);
            switch (choice){
                case 1: getBalance(balance);
                    balance = sendMoney(input,balance);
                    break;
                case 2: balance = requestMoney(input,balance);
                    break;
                case 3: getBalance(balance);
                    break;
                case 4: System.out.println("Goodbye");
                    break;
                default: System.out.println("you entered an invalid value -- try again");
                    break;
            }
        }while(choice != 4);
    }

    public static void printMenu(){
        System.out.println("1. Send Mpney");
        System.out.println("2. Request Money");
        System.out.println("3. Check Balance");
        System.out.println("4. Quit");
    }

    public static int getInt(Scanner scan){
        int choice = -1;
        String trash = "";
        while (!scan.hasNextInt()) {
            trash = scan.nextLine(); //Trash input
            System.out.println("Error: Please type in a valid input");
            printMenu();
        }//Prompts for number of transactions
        choice = scan.nextInt();
        trash = scan.nextLine();
        while (choice < 0 || choice > 4) { //If less than 0 then redoes the cycle
            System.out.println("Error: Please type a positive number");
            printMenu();//Repormpts user
            while (!scan.hasNextInt()) { //Checks again for valid input
                trash = scan.nextLine();
                System.out.println("Error: Please type in a valid input");
                printMenu();
            }
        }
        return choice;
    }


    public static void getBalance (double balance){
        System.out.println("Balance is: " + balance);
    }
    public static double sendMoney(Scanner scan, double balance){
        double input = 0;
        String trash = "";
        System.out.println("What is the amount you want to send");
        while (!scan.hasNextDouble()) {
            trash = scan.nextLine(); //Trash input
            System.out.println("Error: Please type in a valid input");
            System.out.println("What is the amount you want to send");
        }//Prompts for number of transactions
        input = scan.nextDouble();
        trash = scan.nextLine();
        while (input < 0) { //If less than 0 then redoes the cycle
            System.out.println("Error: Please type a positive number");
            System.out.println("What is the amount you want to send");
            while (!scan.hasNextDouble()) { //Checks again for valid input
                trash = scan.nextLine();
                System.out.println("Error: Please type in a valid input");
                System.out.println("What is the amount you want to send");
            }
        }
        if(balance - input >= 0){
            balance = balance - input;
            getBalance(balance);
            return balance;
        }else{
            System.out.println("Error: Insufficient Funds");
            return 0;
        }
    }
    public static double requestMoney(Scanner scan, double balance){
        double input = 0;
        String trash = "";
        System.out.println("What is the amount you want to request");
        while (!scan.hasNextDouble()) {
            trash = scan.nextLine(); //Trash input
            System.out.println("Error: Please type in a valid input");
            System.out.println("What is the amount you want to request");
        }//Prompts for number of transactions
        input = scan.nextDouble();
        trash = scan.nextLine();
        while (input < 0) { //If less than 0 then redoes the cycle
            System.out.println("Error: Please type a positive number");
            System.out.println("What is the amount you want to request");
            while (!scan.hasNextDouble()) { //Checks again for valid input
                trash = scan.nextLine();
                System.out.println("Error: Please type in a valid input");
                System.out.println("What is the amount you want to request");
            }
        }
        if(input > 0){
            balance = balance + input;
            getBalance(balance);
            return balance;
        }else{
            System.out.println("Error: Can't request negative number");
            return 0;
        }
    }

}

    