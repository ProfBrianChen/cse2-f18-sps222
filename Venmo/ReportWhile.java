  /* Sam Sausville, Yuhan Su, William Barish 
   Venmo Activity
   10/1/18
   Class CSE 2 Section 210
  Loops through Venmo Transactions
  */

import java.util.Scanner;

public class ReportWhile{
  
  public static void main (String args[]){
      Scanner scan = new Scanner(System.in); //Scanner Created
      System.out.println("How many transactions have you had this month");
      String trash = "";
      while (!scan.hasNextInt()) {
          trash = scan.nextLine(); //Trashs input
          System.out.println("Error: Please type in a valid input");
          System.out.println("How many transactions have you had this month");
      }//Prompts for number of transactions
      int numTransactions = scan.nextInt();
      trash = scan.nextLine(); //Makes sure buffer
      // Gets num of transactions
      int startNumTransactions = numTransactions; //Sets the starting point before the while loop
      double totalMoneySent = 0; //Total money spent intialized
      double average; //Average money spent created
      double max = 0; //Max set to a low value
      while(numTransactions > 0){ //Loopp starts and contiunes as long as num transactions > 0
          System.out.println("How much was the next Transaction on your list"); //Prompts for next transactiomn since we need data and havent learned arrays quite yet as a way
          //of storing data
          while (!scan.hasNextDouble()) {
              trash = scan.nextLine(); //Trash input
              System.out.println("Error: Please type in a valid input");
              System.out.println("How much was the next Transaction on your list");
          }//Prompts for number of transactions
          double next =  scan.nextDouble();
          trash = scan.nextLine(); //Makes sure buffer //Gets the pormpted user value
          totalMoneySent = totalMoneySent + next; //Adds to toal
          if(next > max){//Checks for new max
              max = next; //Sets new max if there is one
          }
          numTransactions--; //Increments down to make sure the loop doesnt run forever
      }
      average = totalMoneySent/ startNumTransactions; //Calculates average
      System.out.println("Average: " + average); //Prints all the values
      System.out.println("Max: " + max);
      System.out.println("Total Value: " + totalMoneySent);
  }
}