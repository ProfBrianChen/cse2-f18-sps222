public class VenmoVariables{
  
  /*
  Sam Sausville, Bill Barish, Melody Su (Yuhan)
  CSE 2: Sectiom 210
  September 3rd, 2018
  */
  
  public static void main (String args[]){
    int userBankID1 = 1234; //Bank Id of first user
    int userBankID2 = 2345;// Bank id of second user
    double userBankAccountBalance1 = 20.0;//user 1 balance
    double userBankAccountBalance2 = 30.0;//user 2 balance
    int userVenmoID1 = 1;//user 1 venmo id
    int userVenmoID2 = 2; //user 2 venmo id
    double transferAmount = 10;//tranfer amount requested
    double transferFee = 1;//fee for transaction
    double newBalance1 = userBankAccountBalance1 - transferAmount - transferFee;//new balance of user 1
    System.out.println("User 1 Bank ID: " + userBankID1); //Prints all the varibles listed above
        System.out.println("User 2 Bank ID: " + userBankID2);
        System.out.println("User 1 Bank Balance: " + userBankAccountBalance1);
        System.out.println("User 2 Bank Balance: " + userBankAccountBalance2);
        System.out.println("User 1 Venmo ID: " + userVenmoID1);
        System.out.println("User 2 Venmo ID: " + userVenmoID2);
        System.out.println("Transfer Amount: " + transferAmount);
        System.out.println("Transfer Fee: " + transferFee);
        System.out.println("Balance of User 1 after transaction: " + newBalance1);

  }
}