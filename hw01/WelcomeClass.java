public class WelcomeClass{
  /* Sam Sausville 
   Homework 01
   9/4/18
   Class CSE 2 Section 210
   Welcomes the class and user with a special messsage
  */
  
  public static void main (String args[]){
         System.out.println("  ------------");
         System.out.println("  | WELCOME |");
         System.out.println("  ------------");
         System.out.println("  ^  ^  ^  ^  ^  ^");
         System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
         System.out.println("<-S--P--S--2--2--2->");
         System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
         System.out.println("  v  v  v  v  v  v");
    
    System.out.println("\n Hello, my name is Sam Sausville, and I am a CSB major at Lehigh Univeristy. I enjoy concerts, video games and follow all the major sports.");
  }
}