  /* Sam Sausville 
   Homework 02
   9/10/18
   Class CSE 2 Section 210
   Caculates cost of items at a store including sales tax
  */

import java.text.DecimalFormat; // improrted to do decimal formating

public class Arithmetic{
  
  public static void main (String args[]){
    
    DecimalFormat format = new DecimalFormat("##.00"); //Easier way to decimal formatting that I looked up

    
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfShirts; // cost of all the shirts bought
    double totalCostOfPants; // cost of all the pants bought
    double totalCostOfBelts; //cost of all the belts bought
    double salesTaxOfAllPants; // sales tax of all the pants
    double salesTaxOfAllShirts; // sales tax of all the shirts
    double salesTaxOfAllBelts; // sales tax of all the belts
    double totalCost; // total cost
    double totalTax; //total tax
    double totalAll; // total of all of the costs plus tax
      
    totalCostOfPants = numPants * pantsPrice; // number of pants times price to get total pants cost
     salesTaxOfAllPants = paSalesTax * totalCostOfPants; // total pants cost times pa sales tax
    
    totalCostOfShirts = numShirts * shirtPrice; // number of shirts times price to get total shirt costs
     salesTaxOfAllShirts = paSalesTax * totalCostOfShirts; //total shirts cost times pa sales tax
    
    totalCostOfBelts = numBelts * beltCost; // number of belts times price to get total belt costs
     salesTaxOfAllBelts = paSalesTax * totalCostOfBelts; //total belts cost times pa sales tax
    
    totalCost = totalCostOfBelts + totalCostOfShirts + totalCostOfPants; // total cost of all the purchases
    
    totalTax = totalCost * paSalesTax; //Total tax of all the purchaes
    
    totalAll = totalCost + totalTax; //total cost will tax
    
    System.out.println("Total Cost of Pants: " + format.format(totalCostOfPants)); //All print out the double vaules from above, goes in order of how the 
    System.out.println("Total Cost of Shirts: " + format.format(totalCostOfShirts)); // varibles are listed above
    System.out.println("Total Cost of Belts: " + format.format(totalCostOfBelts));

    System.out.println("Total Sales tax of Pants: " + format.format(salesTaxOfAllPants));
    System.out.println("Total Sales tax of Shirts: " + format.format(salesTaxOfAllShirts));
    System.out.println("Total Sales tax of Belts: " + format.format(salesTaxOfAllBelts));

    System.out.println("Total Cost of all before tax: " + format.format(totalCost));

    System.out.println("Total tax of all: " + format.format(totalTax));

    System.out.println("Total Cost of purchase: " + format.format(totalAll));

    

  }
}