  /* Sam Sausville 
   Homework 03
   9/17/18
   Class CSE 2 Section 210
   
   Converts hurricane rain fall and spreads it across acres and gets cubic miles of rain
  */

import java.util.Scanner; //import scanner to use

public class Convert{
  public static void main (String args[]){
    
    Scanner scan = new Scanner (System.in); //Scanner is called scan
    
    System.out.print("Enter the area effected by hurricane in acres:  "); //Prompts user to enter in area effected by rain in acres
    double acres = scan.nextDouble(); //Gets user input
    System.out.print("Enter rainfall in area:  "); //Prompts user to enter in inches of rainfall
    double rainfall = scan.nextDouble(); //Gets user input
    double gallons = acres * (rainfall * 27154); //Calculates gallons in rain based on acres and inches of rain
    System.out.println("Gallons of rainfall:  " + gallons); //Prints out gallons of rain
    double cubicMiles = 0.00000000000090817 * gallons; //Calculates cubic miles using gallons
    System.out.println (cubicMiles + " Cubic Miles" ); //Prints cubic miles
    //Bit confused on the cubic miles output on assignment, I get a different output than that but when I plug all my varibles in the calculatiors
    //my output is correct so I went by that and am just going to hope it is right
  }
}