  /* Sam Sausville 
   Homework 03
   9/17/18
   Class CSE 2 Section 210
   Calculates the volume of a pyramid
  */

// import java.text.DecimalFormat; //How you import another decmial format

import java.util.Scanner; //import scanner to use

public class Pyramid{
  public static void main (String args[]){
    
    Scanner scan = new Scanner (System.in); //Implements the scanner to get input
        //DecimalFormat format = new DecimalFormat("##.00"); // Other decimal format setup so you can use it

    
     System.out.print("Square side of the pyramid is (Input.java length):  "); //Prompts user for square base sides of the pyramid
     int length = scan.nextInt(); //Gets user input
     System.out.print("The height of the pyramid is (Input.java Height):  "); //Prompts user for pyramid height
     int height = scan.nextInt(); //Gets user input
     double volumeCalculation = (.33333333333) * (length * length) * height; //Calculates the volume with a double to get the one third right
     int volume = (int) Math.round(volumeCalculation); //Converts the calculation to an interger and rounds it either up or down depedning on the decimal
     //Could easily change this to decimal simply by importing a decimal format, then making the double adhere to that format
    //That option is left in comments of code
    
        //System.out.println("The volume of the pyramid is: " + format.format(volumeCalculation));

     System.out.println("The volume of the pyramid is:  " + volume); // Prints out final volume

    } 
  }