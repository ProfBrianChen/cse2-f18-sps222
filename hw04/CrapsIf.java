  /* Sam Sausville 
   Homework 04
   9/25/18
   Class CSE 2 Section 210
   Gives outcome names for the game of craps using only if statements
  */

import java.util.Random;
import java.util.Scanner;

public class CrapsIf{
  
  public static void main (String args[]){

        Random rand = new Random(); //Random generator
        Scanner scan = new Scanner(System.in); //Scanner to read in
        System.out.println("Type 1 for Random dice, Type 2 to pick your dice"); //Prompts user input that could be in boolean form but allows for more expansion with numbers
        int check = scan.nextInt(); //Gets check int
        int dice1 = 0;
        int dice2 = 0; //Declares dice 1 and 2
        if(check == 1){ //Checks if user typed in 1
            dice1 = rand.nextInt(5) + 1; //Gets random number for dice 1 and 2
            dice2 = rand.nextInt(5) + 1;
        }else if (check == 2){ //Checks if user typed in 2
            System.out.println("What is dice #1"); //Asked for user input for dice 1
            dice1 = scan.nextInt(); //Get user input
            if(dice1 <= 0 || dice1 >= 7){ //Makes sure it is in range
                System.out.println("Error: You are outside the range");
            }
            System.out.println("What is dice #2"); //Asks for user input for dice 2
            dice2 = scan.nextInt(); //Gets user input
            if(dice2 <= 0 || dice2 >= 7){ //Checks to make sure it is in range
                System.out.println("Error: You are outside the range");
            }
        }else{
            System.out.println("Error"); //Makes sure that a viavble out put had been provided, otherwise error
        }

        String SE = "Snake Eyes"; //Declares all the strings I will be printing out
        String AD = "Ace Deuce";
        String E4 = "Easy Four";
        String FF = "Fever Five";
        String E6 = "Easy Six";
        String SO = "Seven Out";
        String H4 = "Hard Four";
        String H6 = "Hard Six";
        String E8 = "Easy Eight";
        String H8 = "Hard Eight";
        String N = "Nine";
        String ET = "Easy Ten";
        String HT = "Hard Ten";
        String YO = "Yo-leven";
        String BOX = "Boxcars";
    
             System.out.println(dice1 + " " + dice2); //Prints out the dice numbers in case user wants them, not the total like craps switch though, the seperate numbers
            
        if(dice1 == 1){ //Checks for user dice # 1 for what vaule that is, in this case 1
            if(dice2 == 1 ){//Checks for user dice #2 for what value it is, int this case 1
                System.out.println(SE); //Did System.out.println for every option for this time just to prove you can just print this way
              //In CrapsSwitch I set a string that prints out at the end so I know that there is another, probably better way to do it but different mostly
            }else if(dice2 == 2){
                System.out.println(AD); //Example output of this would print out Ace Deuce since its a 1 and a 2, and prints out within the if statement
            }else if(dice2 == 3){
                System.out.println(E4);
            }else if (dice2 == 4){
                System.out.println(FF);
            }else if (dice2 == 5){
                System.out.println(E6);
            }else if (dice2 == 6){
                System.out.println(SO);
            }else{
             System.out.println("Error: Out of range"); //Error check i probably dont need but included in all of them
            }
        }else if (dice1 == 2){ //Same pattern of nested if statements but checking with dice1 = 2
            if(dice2 == 1 ){ //Same pattern of checking all the values of dice 2 in relation to dice 1
                System.out.println(AD);
            }else if(dice2 == 2){
                System.out.println(H4);
            }else if(dice2 == 3){
                System.out.println(FF);
            }else if (dice2 == 4){
                System.out.println(E6);
            }else if (dice2 == 5){
                System.out.println(SO);
            }else if (dice2 == 6){
                System.out.println(E8);
            }else{
             System.out.println("Error: Out of range");
            }

        }else if (dice1 == 3){ //Same pattern
            if(dice2 == 1 ){ //same pattern
                System.out.println(E4);
            }else if(dice2 == 2){
                System.out.println(FF);
            }else if(dice2 == 3){
                System.out.println(H6);
            }else if (dice2 == 4){
                System.out.println(SO);
            }else if (dice2 == 5){
                System.out.println(E8);
            }else if (dice2 == 6){
                System.out.println(N);
            }else{
             System.out.println("Error: Out of range");
            }

        }else if (dice1 == 4){ //Again nothing new but the numbers slightly changed
            if(dice2 == 1 ){ //Numbers the same, different values being potetnially printed out
                System.out.println(FF);
            }else if(dice2 == 2){
                System.out.println(E6);
            }else if(dice2 == 3){
                System.out.println(SO);
            }else if (dice2 == 4){
                System.out.println(H8);
            }else if (dice2 == 5){
                System.out.println(N);
            }else if (dice2 == 6){
                System.out.println(ET);
            }else{
             System.out.println("Error: Out of range");
            }

        }else if (dice1 == 5){  //Same format, different number
            if(dice2 == 1 ){ // Same format, different outputs
                System.out.println(E6);
            }else if(dice2 == 2){
                System.out.println(SO);
            }else if(dice2 == 3){
                System.out.println(E8);
            }else if (dice2 == 4){
                System.out.println(N);
            }else if (dice2 == 5){
                System.out.println(HT);
            }else if (dice2 == 6){
                System.out.println(YO);
            }else{
             System.out.println("Error: Out of range");
            }

        }else if (dice1 == 6){ //You get the gist
            if(dice2 == 1 ){ //Same old same old
                System.out.println(SO);
            }else if(dice2 == 2){
                System.out.println(E8);
            }else if(dice2 == 3){
                System.out.println(N);
            }else if (dice2 == 4){
                System.out.println(ET);
            }else if (dice2 == 5){
                System.out.println(YO);
            }else if (dice2 == 6){
                System.out.println(BOX);
            }else{
             System.out.println("Error: Out of range");
            }

        }else{
             System.out.println("Error: Out of range"); //Last error check just in case, prolly not needed but inluded cause I could and wanted an else statement at the end
            }
  }
}