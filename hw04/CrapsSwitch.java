  /* Sam Sausville 
   Homework 04
   9/25/18
   Class CSE 2 Section 210
   Gives outcome names for the game of craps using only switch statements
  */
import java.util.Random;
import java.util.Scanner;

public class CrapsSwitch{
  
  public static void main (String args[]){
 
        Random rand = new Random(); //Random generator
        Scanner scan = new Scanner(System.in); //Scanner for input
        System.out.println("Type 1 for Random dice, Type 2 to pick your dice"); //Creates essentially a boolean statement with an integer. Easier for me
                                   //to just type in 1 or 2 to check instead of true or false. Plus leaves open for more options if program expanded
        int check = scan.nextInt(); //Scans the user input for what type they want, random or assign value
        int dice1 = 0; //Declares dice1
        int dice2 = 0;//declares dice2
        switch(check){
            case 1:
                dice1 = rand.nextInt(5) + 1; //Gets random number between 1-6 
                dice2 = rand.nextInt(5) + 1;//gets random number between 1-6
                break; 
            case 2:
                System.out.println("What is dice #1's value"); //Asks for value for dice 1
                dice1 = scan.nextInt(); //Gets vaule
                switch(dice1){
                    case 1: //Checks if it is out of range, If statement would be wayyy better here but not allowed to use that so have to type out all the cases to do nothing
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    default:
                        System.out.println("Out of Range"); //Error for out of range
                        break;
                }
                System.out.println("What is dice #2's value"); //asks for value for dice 2
                dice2 = scan.nextInt(); //Gets value
                switch(dice2){ //Same as before, inefficient but works
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    default:
                        System.out.println("Out of Range"); //Error for out of range
                        break;
                }

        }
        String SE = "Snake Eyes"; //Declares all the string names that could potentially be used in the game
        String AD = "Ace Deuce";
        String E4 = "Easy Four";
        String FF = "Fever Five";
        String E6 = "Easy Six";
        String SO = "Seven Out";
        String H4 = "Hard Four";
        String H6 = "Hard Six";
        String E8 = "Easy Eight";
        String H8 = "Hard Eight";
        String N = "Nine";
        String ET = "Easy Ten";
        String HT = "Hard Ten";
        String YO = "Yo-leven";
        String BOX = "Boxcars";

        String last = ""; //String that will be equal to what we will print


        switch (dice1){ //Goes through all the cases of dice 1
            case 1: //Within each case is a nested switch statement which goes through all the cases of case 2
                switch (dice2) {
                case 1:
                    last = SE; //For example, since this case is Dice 1 = 1 and dice 2 = 1 the result that the string will equal is Snake eyes which is
                    break;//the printed result for when both dices are 1. This will follow the same pattern for all of them
                case 2:
                    last = AD;
                    break;
                case 3:
                    last = E4;
                    break;
                case 4:
                    last = FF;
                    break;
                case 5:
                    last = E6;
                    break;
                case 6:
                    last = SO;
                    break;
                default:
                    System.out.println("Error: Dice 2 not in range"); //Another error check that probably isnt needed but is there
                    break;
            }
            break;
            case 2: //Same pattern as before, just with 2 as dice 1
                switch (dice2) {
                    case 1: //Continues through all the possible options of dice 2 paired with dice 1 == 2
                        last = AD;
                        break;
                    case 2:
                        last = H4;
                        break;
                    case 3:
                        last = FF;
                        break;
                    case 4:
                        last = E6;
                        break;
                    case 5:
                        last = SO;
                        break;
                    case 6:
                        last = E8;
                        break;
                    default:
                        System.out.println("Error: Dice 2 not in range");
                        break;
                }
                break;
            case 3: //Same pattern as before
                switch (dice2) {
                    case 1: //Same pattern as before
                        last = E4;
                        break;
                    case 2:
                        last = FF;
                        break;
                    case 3:
                        last = H6;
                        break;
                    case 4:
                        last = SO;
                        break;
                    case 5:
                        last = E8;
                        break;
                    case 6:
                        last = N;
                        break;
                    default:
                        System.out.println("Error: Dice 2 not in range");
                        break;
                }
                break;
            case 4: //Same pattern as before
                switch (dice2) {
                    case 1: //Same pattern as before
                        last = FF;
                        break;
                    case 2:
                        last = E6;
                        break;
                    case 3:
                        last = SO;
                        break;
                    case 4:
                        last = H8;
                        break;
                    case 5:
                        last = N;
                        break;
                    case 6:
                        last = ET;
                        break;
                    default:
                        System.out.println("Error: Dice 2 not in range");
                        break;
                }
                break;
            case 5: //You can guess what this is
                switch (dice2) {
                    case 1: //Nothing different here but the string values
                        last = E6;
                        break;
                    case 2:
                        last = SO;
                        break;
                    case 3:
                        last = E8;
                        break;
                    case 4:
                        last = N;
                        break;
                    case 5:
                        last = HT;
                        break;
                    case 6:
                        last = YO;
                        break;
                    default:
                        System.out.println("Error: Dice 2 not in range");
                        break;
                }
                break;
            case 6: //Again, same
                switch (dice2) {
                    case 1: //Again also same, nothing new but the values being equaled to the string last
                        last = SO;
                        break;
                    case 2:
                        last = E8;
                        break;
                    case 3:
                        last = N;
                        break;
                    case 4:
                        last = ET;
                        break;
                    case 5:
                        last = YO;
                        break;
                    case 6:
                        last = BOX;
                        break;
                    default:
                        System.out.println("Error: Dice 2 not in range");
                        break;
                }
                break;
            default:
                    System.out.println("Error: Dice 1 not in range"); //Another error check for dice 1 that probsbly isnt needed
                    break;
        }
        System.out.println((dice1 + dice2)+ " " + last); //Prints out the dice number total to check, not the seperate numbers like crapsIf
    //and then the string attached to whatever was rolled and what exactly it is called
  }
}