/* Sam Sausville
 Hw 05
 10/9/18
 Class CSE 2 Section 210
 Twisted Generator that figures out a certain output to print
*/

import java.util.Scanner;
public class TwistGenerator{
    public static void main (String args[]){
        Scanner scan = new Scanner(System.in); //Scanner set
        System.out.println("What is the length"); //Prompts user for length
        String trash = ""; //Trash variable
        while(!scan.hasNextInt()){ //Error check loop
            trash = scan.nextLine(); //Trashs
            System.out.println("Error: Please type in a valid input"); //Repromts the user with error message and asks again for length
            System.out.println("What is the length");
        }
        int length = scan.nextInt(); //Gets length
        trash = scan.nextLine(); //Clears buffer
        while(length < 0){ //If less than 0 then redoes the cycle
            System.out.println("Error: Please type a positive number"); //Repormpts user
            System.out.println("What is the length");
            while(!scan.hasNextInt()){ //Checks again for valid input
                trash = scan.nextLine();
                System.out.println("Error: Please type in a valid input");
                System.out.println("What is the length");
            }
            length = scan.nextInt();//Gets length again, exits as long as the interger is postive
            trash = scan.nextLine();//Clears buffer
        }
        int count = 0; //Variable for all the prints
        int length1 = length; //Keeps track of the first loop
        while (length1 >= 0 ){
            if(count == 0){ //If first, prints out the first backslashs
                System.out.print("\\");
                count++; //Adds count
            }else if (count == 1){
                System.out.print(" "); //Next is space
                count++;
            }else if (count == 2){ //next is a reguller /
                System.out.print("/");
                count++;
            }else{
                count = 0; //Resets to 0 to go through the cycle again
                length1++; //makes sure length isnt taken into account for this step
            }
            length1--;
        }
        int check = 0; //Checks for futre loop to make sure the X is printed out in the right time
        if((length + 1) % 3 == 0){ //Basically changes the loop to make it right, if is on the 3rd, print out the X
            check = 1; // To print out x
        }else{
            check = 2; // To not print out x
        }
        System.out.println(); //Next line
        int length2 = length; //New length, probably could have done the same varible just reset it but I think this is a bit cleaner
        count = 0; //Reset to 0
            while (length2 >= check ){ //Is set to stop at 2 or 1 to get x to print out at the right time
            if(count == 0){
                System.out.print(" "); //Same idea, just a bit different since loop is based off the check variable
                count++;
            }else if (count == 1){
                System.out.print("X");
                count++;
            }else if (count == 2){
                System.out.print(" ");
                count++;
            }else{
                count = 0;
                length2++;
            }
            length2--;
        }
        System.out.println(); // New line
        int length3 = length; //Rest is just same as before
        count = 0;
        while (length3 >= 0 ){
            if(count == 0){
                System.out.print("/");
                count++;
            }else if (count == 1){
                System.out.print(" ");
                count++;
            }else if (count == 2){
                System.out.print("\\");
                count++;
            }else{
                count = 0;
                length3++;
            }
            length3--;
        }
    }

}

