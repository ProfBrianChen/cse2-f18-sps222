import java.util.Scanner;

public class EncryptedX {

    /* Sam Sausville
 Homework 06
 10/23/18
 Class CSE 2 Section 210
 Prints out a grid with an X in the middle
*/

        public static void main (String args[]) { //Lot of the comments are reused and stuff from my previous projects and homework since
            //It is the same idea and concept so its easy just to use the same varible names as my previous onews
            Scanner scan = new Scanner(System.in); //Scanner set
            System.out.println("What is the size of the grid between 0-100"); //Prompts user for size of grid
            String trash = ""; //Trash variable
            while (!scan.hasNextInt()) { //Error check loop
                trash = scan.nextLine(); //Trash
                System.out.println("Error: Please type in a valid input"); //Repromts the user with error message and asks again for length
                System.out.println("What is the size of the grid between 0-100"); //Prompts user for size of grid
            }
            int length = scan.nextInt(); //Gets length
            trash = scan.nextLine(); //Clears buffer
            while (length < 0 || length > 100) { //If less than 0 or greater than 100 then redoes the cycle
                System.out.println("Error: Please type a positive number"); //Repormpts user
                System.out.println("What is the size of the grid between 0-100"); //Prompts user for size of grid
                while (!scan.hasNextInt()) { //Checks again for valid input
                    trash = scan.nextLine();
                    System.out.println("Error: Please type in a valid input");
                    System.out.println("What is the size of the grid between 0-100"); //Prompts user for size of grid
                }
                length = scan.nextInt();//Gets length again, exits as long as the interger is postive
                trash = scan.nextLine();//Clears buffer
            }
//length /2 if i wanted to make it two loops
            for (int i = 0; i < length; i++) { //Starts with first half of the grid, first loop is rows
                for (int j = 0; j < length; j++) { //Nested for spaces //Equals if you want too loops as well
                    if (j == length - (i)-1 || j == i) { //If it is in a certain row based on i, then change where the space is
                        System.out.print(" "); //Add space
                    } else {
                        System.out.print("*");// Add star
                    }
                }
                System.out.println(); //Next line
            }

         /*   for(int i = length/2; i >= 0; i--){ //Same idea, but with second half as well as the middle part of the loop
                for (int j = 0; j <= length; j++){ //Nested for spaces and stars, otherwise known as columns
                   if(j == length - i || j == i){ //Same idea of depedning on x add space
                       System.out.print(" "); //Add space

                   }else{
                       System.out.print("*"); //Add star
                   }
                }
                System.out.println(); //Next line then done
            }
        }*/
        }

}
