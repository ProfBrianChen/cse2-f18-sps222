
import java.util.Scanner;

public class WordTools {
      /* Sam Sausville
   Homework 07
   10/30/18
   Class CSE 2 Section 210
   Homework 7 word tools
  */
      public static void main (String args[]){
          Scanner scan = new Scanner (System.in); //Scanner set
         String sample =  sampleText(scan); //Sample string method get
         char option = 'a'; //Option char intialized with a cause why not
         do {
             option = printMenu(scan); //Prints menu and returns option char
             if (option == 'c') {
                 System.out.println("Number of characters: " + getNumOfNonWSCharacters(sample)); //Methods explain for themsevles what they do
             } else if (option == 'w') {
                 System.out.println("Number of words: " + getNumOfWords(sample));
             } else if (option == 'f') {
                 System.out.println("Please type in another string");
                String yeet = scan.nextLine();
                 System.out.println("Instances found: " + findText(sample, yeet));
             } else if (option == 'r') {
                 System.out.println(replaceExclamation(sample));
             } else if (option == 's'){
                 System.out.println(shortenSpace(sample));
             }
             System.out.println();

         }while(option != 'q'); //Quits if need be
          System.out.println("Buh Bye"); //Ending message
      }

      public static String sampleText(Scanner scan){
          System.out.println("Enter in sample text: "); //asks for sting from uers
          String quote = "";
          String next = scan.nextLine(); //Get string, doesnt matter what it is
          System.out.println("Sample text: " + next); //Prints string back
          System.out.println();
          return next; //Returns stirng
      }

      public static char printMenu (Scanner scan){ //
          String trash = "";
          System.out.println("c - Number of non-whitespace characters"); //Menu options
          System.out.println("w - Number of words");
          System.out.println("f - Find text");
          System.out.println("r - Replace all !'s");
          System.out.println("s - Shorten spaces");
          System.out.println("q - Quit");
          System.out.println();
          System.out.println("Choose an Option");

          while(!scan.hasNext("c") && !scan.hasNext("w") && !scan.hasNext("f") //Error check for good data, reprompts if bad
                  && !scan.hasNext("r") && !scan.hasNext("s") && !scan.hasNext("q")){
              System.out.println("Error: Please type in valid input");
              trash = scan.nextLine();
              System.out.println("c - Number of non-whitespace characters");
              System.out.println("w - Number of words");
              System.out.println("f - Find text");
              System.out.println("r - Replace all !'s");
              System.out.println("s - Shorten spaces");
              System.out.println("q - Quit");
              System.out.println();
              System.out.println("Choose an Option");
          }
          String str = scan.next();// Gets string needed for entering
          char c = str.charAt(0); //Converts to char
          trash = scan.nextLine(); //Trash just in case
          return c; //Returns char which will be used for options
      }

      public static int getNumOfNonWSCharacters(String s){
          int count = 0;
         for(int i = 0; i < s.length(); i++){ //Sets up to count each charcter
             char c = s.charAt(i);
             if(c != ' '){ //Excludes spaces
                 count++;
             }
         }
         return count; //Returns the count
      }

      public static int getNumOfWords(String s){
          int count = 1; //Starts at one since there will always be one word
          for(int i = 0; i < s.length(); i++){ //Counts spaces between words
              char c = s.charAt(i);
              if(c == ' ' && s.charAt(i -1) != ' '){ //Makes sure to exlude if there are more than one space between a word
                  count ++;
              }
          }
          return count; //Returns the count
      }

      public static int findText(String s, String k){
          int place = 0; //Sets the index
          int count = 0;
            while(place != -1){ //is the loop that makes sure the seocond string exists
             place = s.indexOf(k, place); //changes the place to wherever the desired thing is found
             if(place != -1){ //Only adds if string is found to exsit
                 count++;
                 place = place + k.length(); //Sets place to after all that has already been checked
             }
         }
        return count; //Returns the count
      }

      public static String replaceExclamation(String s){ //Confused by what it meant by not output the string, so just assumed that i meant
          //dont change the physical string, so just chanced another and returned it
          String other = s.replace('!', '.'); //Simple command that replaces all ! with periods
          return other;
      }

      public static String shortenSpace(String s){ //Same confusion on this one with the last one
          String other = s.trim().replaceAll("\\s{2,}", " "); //Trims down if the spaces are too many, and replaces with single ones
          return other;
      }

      }
