/*
  /* Sam Sausville
   Homework 08
   10/30/18
   Class CSE 2 Section 210
   Homework 8 cards and arrays
  */
import java.util.Random;
import java.util.Scanner;
public class Shuffling{
    public static void main(String[] args) { //Didnt touch rest of the code
        Scanner scan = new Scanner(System.in);
        //suits club, heart, spade or diamond
        String[] suitNames={"C","H","S","D"};
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
        String[] cards = new String[52];
        String[] hand = new String[5];
        int numCards = 5;
        int again = 1;
        int index = 51;
        for (int i=0; i<52; i++){
            cards[i]=rankNames[i%13]+suitNames[i/13];
            System.out.print(cards[i]+" ");
        }
        System.out.println();
        printArray(cards);
        shuffle(cards);
        printArray(cards);
        while(again == 1){
            if(numCards > index){ //if statement if the deck basically runs out to re shuffle and reset the index
                shuffle(cards);
                printArray(cards);
                index = 51;
            }
            hand = getHand(cards,index,numCards);
            printArray(hand);
            index = index - numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }
    public static void printArray(String [] arr){ //For loop that just prints the entire array
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();

    }
    public static void shuffle(String [] arr){ //Shuffle based on how it is described, set a random amount of times
        Random rand = new Random();
        int timesSwap = rand.nextInt(50) + 50; //Made sure it is at least 50 but then can be up to double that amount
        for(int i = 0; i < timesSwap; i++){
            int indexRand = rand.nextInt(52); //Takes random index
            String temp = arr[indexRand]; //Stores temp of array at that index
            arr[indexRand] = arr[0]; //sets the starting index to the random index value
            arr[0] = temp; //Sets starting to what that value was, creating the swap
        }
    }
    public static String [] getHand(String [] arr, int index, int numCards){ //Pulls cards down
        String [] hand = new String[numCards]; //create new array
        for(int i = 0; i < numCards; i++){ //Go through the new array based on num cards needed to pull
            hand[i] = arr[index]; //Pull based on the index down
            index--; //Make sure to increment index too
        }
        return hand;
    }
}
