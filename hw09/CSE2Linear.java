/* Sam Sausville
   Homework 09
   11/26/18
   Class CSE 2 Section 210
   Homework 9 Binary and Linear search
  */

import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 15 ascending ints for final grades in CSE2:"); //Goes number by number to allow for
        //Easier error checking for me
        String trash = "";
        int grade = 0; //Main way to track what is happening
        int[] grades = new int[15];
       // int [] grades2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}; just a way to test searches faster
        for (int i = 0; i < grades.length; i++) { //Reads in an entire array of ints before checking all of them so
            //I can always have a valid input
            //Does it seperatly since that was easier to implement
            System.out.println("Enter grade number " + (i +1) + " "); //Goes up to 15 for each number you enter
            while (!scan.hasNextInt()) { //Error check for input, allows user to renter
                trash = scan.nextLine();
                System.out.println("Invalid Input please try again"); //Error type #1
                System.out.println("Enter grade number " + (i +1) + " ");
            }
            grade = scan.nextInt();
            trash = scan.nextLine();
            grades[i] = grade;

        }
        printArray(grades);  //Prints array that currently exist for the user
        boolean check; //Boolean to check if the array is correct for a binary search
        do{ // start of the loop, has to be done at least once, one of the few cases imo that a do while is better
            //Than just a while loop
            check = true; //Sets the boolean to true until it is found to be not true in the check later
            for(int i = 0; i < grades.length; i++){ //Goes through one by one and error checks
                if(i != 0){ //Makes sure there is no out of bounds exception when checking the number before
                    if(grades[i] > 100 || grades[i] < 0){ //Checks that it is in range
                        System.out.println("Your grade number was out of range," + //Error type #2
                                " enter grade number " + (i +1) + " "); //Remproimpts the user exactly where the
                        //Mistake is located and to reenter the number, they can look back at the array to determine
                        while (!scan.hasNextInt()) { //Makes sure valid input still
                            trash = scan.nextLine();
                            System.out.println("Invalid Input please try again"); //Error type #1
                            System.out.println("Enter grade number " + (i +1) + " ");
                        }
                        grade = scan.nextInt();
                        trash = scan.nextLine();
                        grades[i] = grade;//Puts the new number in the array
                    }else if(grades[i-1] > grades[i]){ // Checks that it is in order
                        System.out.println("Your grade number was not in order," + //Error type #3
                                " enter grade number " + (i +1) + " "); //Remprompts user exactly where error is
                        //Can look at array to fix, but still is protected if they screw up again
                        while (!scan.hasNextInt()) {//Makes sure valid input still
                            trash = scan.nextLine();
                            System.out.println("Invalid Input please try again"); //Error type #1
                            System.out.println("Enter grade number " + (i +1) + " ");
                        }
                        grade = scan.nextInt();
                        trash = scan.nextLine();
                        grades[i] = grade; //Again fills in the grade
                    }
                }else{
                    if(grades[i] > 100 || grades[i] < 0){ //Just does the first part if its the first element in the
                        //Array
                        System.out.println("Your grade number was out of range," +
                                " enter grade number " + (i +1) + " ");
                        while (!scan.hasNextInt()) {
                            trash = scan.nextLine();
                            System.out.println("Invalid Input please try again"); //Error type #1
                            System.out.println("Enter grade number " + (i +1) + " ");
                        }
                        grade = scan.nextInt();
                        trash = scan.nextLine();
                        grades[i] = grade;

                    }
                }
            }
            for (int i = 0; i < grades.length; i++){ //This loop goes through the array again and makes sure everything
                //is correct
                if(i != 0){ //Again, avoiding out of bounds
                    if(grades[i] > 100 || grades[i] < 0 || grades[i-1] > grades[i]){
                        check = false; //if it isnt, check goes to being false the the loop is exitied
                        break;
                    }
                }else{
                    if(grades[i] > 100 || grades[i] < 0){
                        check = false;
                        break;
                    }
                }
            }
            printArray(grades); //Prints the updated array
        }while(check == false); //Only continues if check is false and there is more mistakes
        System.out.println("Enter in a target number to search for"); //Gets target number to search
        while (!scan.hasNextInt()) { //Input check
            trash = scan.nextLine();
            System.out.println("Invalid Input please try again");
            System.out.println("Enter in a target number to search for");
        }
        int target = scan.nextInt();
        trash = scan.nextLine();
        binarySearch(grades, target); //Does binary search for it

        scramble(grades); //Scrambles the array
        printArray(grades); //Prints the new array
        System.out.println("Enter in a target number to search for"); //Valid input check for target again
        while (!scan.hasNextInt()) {
            trash = scan.nextLine();
            System.out.println("Invalid Input please try again");
            System.out.println("Enter in a target number to search for");
        }
        target = scan.nextInt();
        trash = scan.nextLine();
        linearSearch(grades, target); //Performs linear search

    }
    public static void linearSearch(int[] arr, int tar){
        boolean test = false; //Test to see if grade is found at all
        int count = 0;
        // once
        for(int i = 0; i < arr.length; i++){
            if(arr[i] == tar){ //Checks if target is that
                test = true;//changes boolean to true as we found target
                count = i + 1; //Gets iterations needed
                break;//breaks the loop
            }
        }
        if(test == true){
            System.out.println("Grade was found after "+ count + " iterations"); //If found
        }else{
            System.out.println("Grade was not found"); //If not found
        }
    }

    public static void scramble(int [] arr){ //Simple scrable method, did it just like the shuffle method in the
        //previous hw assignment
        Random rand = new Random();
        int swapAmt = rand.nextInt(50) + 15; //Makes sure to shuffle at least 15 times
        for(int i = 0; i < swapAmt; i++){
            int randIndex = rand.nextInt(arr.length);
            int temp = arr[randIndex];
            arr[randIndex] = arr[0];//Perfoms the swap
            arr[0] = temp;
        }
    }

    public static void binarySearch(int[] arr, int tar){ //Main binary search method that is used
        int start = 0;
        int end = arr.length-1;
        int get = -1; //Is -1 to represent in case the number is not found
        int count = 0; //Counts interations
        while (start <= end){
            int mid = start + (end-start) /2; //Starts at midpoint
            if(arr[mid] == tar){ //If equal sets get to the target vaule since its found and breaks the loop
                get = tar;
                break;
            }else if(arr[mid] > tar){ //If tar is less than then bring the end equal to the mid point
                end = mid-1;
            }else{ //If tar is greater than bring start to the mid point
                start = mid+1;
            }
            count++; //Count iterations
        }
        if(get == -1){ //If not found
            System.out.println(tar + " could not be found after " + count + " iterations");
        }else{ //If found
            System.out.println(tar + " was found after " + count + " iterations");
        }

    }

    public static void printArray (int [] arr){ //Simple method that prints the array
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}

