import java.util.Random;
import java.util.Scanner;

/* Sam Sausville
   Homework 09
   11/26/18
   Class CSE 2 Section 210
   Homework 9 Binary and Linear search
  */
public class RemoveElements{
    public static void main(String [] arg){ //Didnt touch this main method, so wont comment
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
        String answer="";
        do{
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]){ //Didnt make this method either, so wont comment
        String out="{";
        for(int j=0;j<num.length;j++){
            if(j>0){
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }
    public static int[] randomInput(){ //Randomizes an array of 10, pretty simple stuff
        int [] arr = new int[10];
        Random rand = new Random();
        for(int i = 0; i < arr.length; i++){
            arr[i] = rand.nextInt(10);
        }
        return arr;
    }
    public static int [] delete (int  [] arr, int index){ //Removes a certain index from the array and returns a new one
        if(index < 10 && index >= 0){ //Makes sure index is in range
            int [] newarr = new int[arr.length-1];
            for (int i = 0; i < index; i++){ //Goes until the desired index
                newarr[i] = arr[i];
            }
            for(int i = index; i < newarr.length; i++){ //Then just shifts everynumber down from that index
                newarr[i] = arr[i+1];
            }
            return newarr;
        }else {
            System.out.println("Error: Out of range"); //Checks for out of range just in case, and doesnt remove
            //any index as a result
            return arr;
        }
    }
    public static int [] remove(int [] arr, int tar){ //Remove any target from the array
        int count = 0; //First goes through and counts how many times is needed to remove
        for(int i = 0; i < arr.length; i++){
            if(arr[i] == tar){
                arr[i] = -1; //Also change the target numbers to -1 so its easier to identify, although it is not needed
                //neccarily
                count++;
            }
        }
            int [] newarr = new int[arr.length - count]; //Creates the new array based on how many elements were changed
            count = 0; //Changed back to zero to make sure we are filling the new array correclty
            for(int i = 0; i < arr.length; i++){
                if(arr[i] >= 0){ //Only adds elements that are aboce 0
                    newarr[count] = arr[i]; //takes the current count number and sets it to the non -1 number in the
                    //array
                    count++; // iterates to make sure we are on the same pace as the for loop and dont add numbers we
                    //dont need and stay within bounds of our array
                }
            }
            return newarr;

    }
}

