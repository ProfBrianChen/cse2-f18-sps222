/* Sam Sausville
   Homework 10
   12/04/18
   Class CSE 2 Section 210
   Homework 10 tic tac toe
  */

import java.util.Scanner;

public class hw10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner (System.in);
        char [][] board = { {'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'} };//2d Board array
        printBoard(board); //Prints put the current board
        int count = 0; //Keeps track of how many turns have occured
        int win = -1;//Keeps track if there is a winner or not
        char turn = ' '; //Keeps track of whos turn it is
        while(win == -1 && count < 9){ //Makes sure that noboday has won and that we arent over the max turns
            if(count % 2 == 0){ //Checks for even or odd to get turn
                turn = 'O';
            }else{
                turn = 'X';
            }
          int change = validInput(scan, board, turn); //Validates the input to make sure its a valid position
            changeArr(board, change, turn); //Makes the change in the array
            printBoard(board);
            win = checkWinner(board); //Checks for a winner
            count++; //Increments turn
        }
        System.out.println(); //Prints put different winner options
        if(win == 0){
            System.out.println("Team O won the game!!!");
        }else if(win == 1){
            System.out.println("Team X won the Game !!!");
        }else{
            System.out.println("Draw for both Teams");
        }
        printBoard(board);

    }

    public static void changeArr(char[] [] board, int x, char change ){ //Changes the position to a O or X depending
        //on the turn
        char pos = (char) (x + 48); //To make sure the value is displayed right, need to add 48 so it can be a number
        //character, otherwise just grabs the ascii value of the position which doesnt work
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                if(board[i][j] == pos){
                    board[i][j] = change;
                }
            }
        }
    }

    public static int checkWinner (char [] [] board){ //Does all the different win checks
        int win1 = winHor(board);
        int win2 = winVer(board);
        int win3 = winDiaL(board);
        int win4 = winDiaR(board);
        if(win1 == 1 || win2 == 1 || win3 == 1|| win4 == 1){ //Checks if any are true for X
            return 1;
        }else if(win1 == 0 || win2 == 0 || win3 == 0|| win4 == 0){ //checks if any are true for O
            return 0;
        }else{ //Otherwise not true
            return -1;
        }
    }

    public static int winHor (char [] [] board){ //Checks the if there are 3 in a row horizontally
        int trackO = 0;
        int trackX = 0;
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                if(board[i][j] == 'X'){ //Increment depending on if it equals the right char
                    trackX++;
                }else if(board[i][j] == 'O'){
                    trackO++;
                }
            }
            if(trackX == 3){ //Return who won if three in a row are found
                return 1;
            }else if(trackO == 3){
                return 0;
            }
            trackO = 0; //Reset to make sure its just rows being counted
            trackX = 0;
        }
        return -1; //If winner not found, return -1
    }

    public static int winVer (char [] [] board){ //Same logic as horizontal, just different order in 2d array
        int trackO = 0;
        int trackX = 0;
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                if(board[j][i] == 'X'){
                    trackX++;
                }else if(board[j][i] == 'O'){
                    trackO++;
                }
            }
            if(trackX == 3){
                return 1;
            }else if(trackO == 3){
                return 0;
            }
            trackO = 0;
            trackX = 0;
        }
        return -1;

    }

    public static int winDiaL (char [] [] board){//Similar to first 2, but a bit easier
        int trackO = 0;
        int trackX = 0;
        for(int i = 0; i < board.length; i++){ //Only need one loop since there is only 1 scenario to check for
            if(board[i][i] == 'X'){
                trackX++;
            }else if(board[i][i] == 'O'){
                trackO++;
            }
        }
        if(trackX == 3){
            return 1;
        }else if(trackO == 3){
            return 0;
        }
        return -1;

    }

    public static int winDiaR (char [] [] board){ //Same as the other diagonal, but other way
        int trackO = 0;
        int trackX = 0;
        for(int i = 0; i < board.length; i++){
            if(board[i][board.length-1-i] == 'X'){ //Make sure to do it from right to left instead of left to right
                trackX++;
            }else if(board[i][board.length-1-i] == 'O'){
                trackO++;
            }
        }
        if(trackX == 3){
            return 1;
        }else if(trackO == 3){
            return 0;
        }
        return -1;

    }

        public static int validInput(Scanner scan, char [] [] board, char c){ //Standard input validation
        int move = inputCorrect(scan, c); //Makes sure input is an int
        boolean empty = checkPosition(c, board, move); //Makes sure position insnt filled
        while(move > 9 || move < 0 || empty == true ){ //If either one is true, try again until it is
            if(move > 9 || move < 0){ //Out of range error
                System.out.println("Out of range of Board, please try again");
            }else if(empty == true){ //Out of positon error
                System.out.println("Postion already taken, please try again");
            }else{ //An error that is unknown but I cant get which is good
                System.out.println("Unknown Error");
            }
            move = inputCorrect(scan, c); //Same methods as before
            empty = checkPosition(c, board, move);
        }
        return move; //Returns the position that needs to be changed
    }

    public static boolean checkPosition(char check, char [] [] board, int move){ //Simple loop that makes sure the position
        //Needed isnt filled, prolly a better way to do this but idk how and this works
        char c = (char) (move+48); //Same idea with the ascii value
        boolean b = true;
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                if(board[i][j] == c){
                    b = false;
                }
            }
        }
        return b;
    }

    public static int inputCorrect(Scanner scan, char c){ //simple method to make sure input in an int and correct if not
        if(c == 'O'){
            System.out.println("O's turn, please type in a postion");
        }else{
            System.out.println("X's turn, please type in a postion");
        }
        String trash = "";
        while(!scan.hasNextInt()){
            System.out.println("Please type in a valid input type");
            trash = scan.nextLine();
        }
        int move = scan.nextInt();
        trash = scan.nextLine();
        return move;
    }
    //Method that simply just prints the board as wanted
    public static void printBoard(char[][] board){
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                System.out.print(board[i][j]);
                System.out.print("   ");
            }
            System.out.println();
        }
    }
}
