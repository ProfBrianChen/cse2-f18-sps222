  /* Sam Sausville 
   Homework 01
   9/4/18
   Class CSE 2 Section 210
   Welcomes the class and user with a special messsage
  */
public class HelloWorld{
  
  public static void main (String args[]){
    //Prints the words "Hello, World" to the terminal window
    System.out.println("Hello, World");
  }
}