
public class Cyclometer{ 
  public static void main (String args[]){
    int secsTrip1=480;  //Seconds in trip 1
   	int secsTrip2=3220;  //seconds in trip 2
		int countsTrip1=1561;  // counts on trip
		int countsTrip2=9037; // counts on trip
    
    double wheelDiameter=27.0;  // wheel diameter
  	double PI=3.14159; // pi
  	double feetPerMile=5280;  // feet in a mile
  	double inchesPerFoot=12;   // inches in a foot
  	double secondsPerMinute=60;  // minutes in a second
	  double distanceTrip1; //Trip 1 distance
    double distanceTrip2;// Trip 2 distance
    double totalDistance;  // addition of distance in trip 1 and 2
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1+ " counts."); //Prints time it took for trip one
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2+ " counts.");// Prints time it took for trip 2
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //distance in inches for each totation wheel travels in diameter
    	
	  distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //Does same computation but into miles agian
	  totalDistance = distanceTrip1 + distanceTrip2; //Adds the two trips
    
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); //Prints distance for trip 1
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles");// Prints distance for trip 2
	  System.out.println("The total distance was " + totalDistance + " miles"); //Prints total distance

	

    



  }
}