  /* Sam Sausville 
   Homework 01
   9/12/18
   Class CSE 2 Section 210
   Calcuates a check for the a table going out
  */

import java.util.Scanner;
// import java.text.DecimalFormat; //How you import another decmial format

public class Check{
  
  public static void main (String args[]){
    
    Scanner scan = new Scanner(System.in);
    
    //DecimalFormat format = new DecimalFormat("##.00"); // Other decimal format setup so you can use it

    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Asks for user input for total cost
    double checkCost = scan.nextDouble();// get user input in right format
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //Asked for user input for tip
    double tipPercent = scan.nextDouble();// gets user input in right format
    tipPercent /= 100; //Makes it a percentagr
    System.out.print("Enter the number of people who went out to dinner: " ); //Gets number of people out to dinner
    int numPeople = scan.nextInt(); //Gets number of people out to dinner
    double totalCost; //Sets total cost
    double costPerPerson; //Set cost per person
    int dollars, dimes, pennies; // Sets int for dollars, times and pennies needed, aka different decimal points
    totalCost = checkCost * (1 + tipPercent); //Gets cost plus tip percentage
    costPerPerson = totalCost / numPeople; //Divides the cost per person, keeping decimal for now
    dollars = (int)costPerPerson; //Gets dollar amount per person
    dimes = (int) (costPerPerson * 10) % 10; //Gets dime amount per person by mutiplying by 10 then pulling that number off
    pennies = (int) (costPerPerson * 100) % 10; //Gets penny amount per person bt mutiplying by 100 then pulling that number off with moduloe
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //Prints final output

  }
}