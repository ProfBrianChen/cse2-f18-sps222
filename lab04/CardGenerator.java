  /* Sam Sausville 
   Lab 04
   9/19/18
   Class CSE 2 Section 210
   Random Card generator for a magicain
  */

import java.util.Random;

public class CardGenerator{
  
  public static void main (String args[]){
    Random rand = new Random();
    int cardNum = rand.nextInt(51) + 1; //Random generator for a number between 1-52
    String suit = ""; // Suit string
    String identity = ""; //Idenity string for number
    System.out.println(cardNum); //Check the number for me
    
    if(cardNum >= 1 && cardNum <= 13){ //For diamonds 1-13
      suit = "Diamonds";
    }else if (cardNum >= 14 && cardNum <= 26){ //For clubs 14-26
      suit = "Clubs";
    }else if (cardNum >= 27 && cardNum <= 39){ //For hearts 27 - 39
      suit = "Hearts";
    }else if (cardNum >= 40 && cardNum <= 52){ //For spades 40-52
      suit = "Spades";
    }
    
    cardNum = cardNum % 13; //Converts number to 0-12
    switch(cardNum){
      case 1: //Ace since returns a 1
      identity = "Ace"; 
      break;
      case 11: //Jack since it returns 11 
      identity = "Jack";
      break;
      case 12: //Queen since it gets a 12
      identity = "Queen";
      break;
      case 0: // Needs to actually be 0 for a number that is 13 since the modulus is 0 when it is 13 for it to be a king
      identity = "King"; //All has to do with the fact that modulus 13 on a number divisable by 13 gives no remainder
      break;
      default: //Defaults to the card number if not any of the other cases
      identity = identity + cardNum;
      break;
    }
    
    System.out.println("You Picked the " + identity + " of " + suit ); //Prints out output
    
  }
}