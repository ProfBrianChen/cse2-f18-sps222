/* Sam Sausville
 Lab 05
 10/3/18
 Class CSE 2 Section 210
 Testing scan.has next statements with while loops
*/
import java.util.Scanner;
public class Input{

    public static void main (String args[]){
        Scanner scan = new Scanner(System.in); //Set Scanner

        String trash = ""; //Trash variable

        int courseNum = 0; // Declares all the varibles that will be used, this is course number
        String deptName = ""; //Dept name string
        int numTimesMeet = 0; // Times to meet
        int timeClass = 0; //time in class
        String profName =  ""; //Prof name
        int numStudents = 0; // Num of students int
        System.out.println("Type in Course Number: ");
        while(!scan.hasNextInt()){ //Checks for next int, if not there goes through loop
            trash = scan.nextLine(); //Trashs input
            System.out.println("Error: Please Type in an Integer Value"); //Says error
            System.out.println();
            System.out.println("Type in the times met this week: "); //Re asks for input again
        }
        courseNum = scan.nextInt();  //If input good scans the input
        trash = scan.nextLine(); //Makes sure buffer i
        System.out.println("Type in Department Name: ");
        while(scan.hasNextInt() || scan.hasNextBoolean() || scan.hasNextDouble() || scan.hasNextFloat()){ //Checks to see if its a different value then string
            trash = scan.nextLine();
            System.out.println("Error: Please Type in an String Value"); //All same code as the other time
            System.out.println();
            System.out.println("Type in Department Name");
        }
        deptName = scan.nextLine(); //Only scans line once since its a string output
        System.out.println("Type in the times met this week: ");
        while(!scan.hasNextInt()){
            trash = scan.nextLine(); //Same code as previous integer chec k
            System.out.println("Error: Please Type in an Integer Value");
            System.out.println();
            System.out.println("Type in the times met this week: ");
        }
        numTimesMeet = scan.nextInt(); //Rest is just the same
        trash = scan.nextLine();
        System.out.println("Type in Class Time: ");
        while(!scan.hasNextInt()){
            trash = scan.nextLine();
            System.out.println("Error: Please Type in an Integer Value");
            System.out.println();
            System.out.println("Type in Class Time: ");
        }
        timeClass = scan.nextInt();
        trash = scan.nextLine();
        System.out.println("Type in Prof Name: ");
        while(scan.hasNextInt() || scan.hasNextBoolean() || scan.hasNextDouble() || scan.hasNextFloat()){
            trash = scan.nextLine();
            System.out.println("Error: Please Type in an String Value");
            System.out.println();
            System.out.println("Type in Prof Name: ");
        }
        profName = scan.nextLine();
        System.out.println("Type in the number of students in the class: ");
        while(!scan.hasNextInt()){
            trash = scan.nextLine();
            System.out.println("Error: Please Type in an Integer Value");
            System.out.println();
            System.out.println("Type in the number of students in the class: ");
        }
        numStudents = scan.nextInt();

        System.out.println("Course Number: " + courseNum); //Prints all outputs to double
        System.out.println("Department Name: " + deptName);
        System.out.println("Times met in a week: " + numTimesMeet);
        System.out.println("Class Time: " + timeClass);
        System.out.println("Professor Name: " + profName);
        System.out.println("Number of Students: " + numStudents);
    }
}