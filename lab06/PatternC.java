
/* Sam Sausville
 Lab 05
 10/10/18
 Class CSE 2 Section 210
 Creating Loop Patterns
*/
import java.util.Scanner;

public class PatternC {
    public static void main (String args[]){
        Scanner scan = new Scanner(System.in); //Scanner set
        System.out.println("How many rows"); //Prompts user for rows
        String trash = ""; //Trash variable
        while(!scan.hasNextInt()){ //Error check loop
            trash = scan.nextLine(); //Trashs
            System.out.println("Error: Please type in a valid input"); //Repromts the user with error message and asks again for rows
            System.out.println("How many rows");
        }
        int rows = scan.nextInt(); //Gets rows
        trash = scan.nextLine(); //Clears buffer
        while(rows < 1 || rows > 11){ //If less than 0 or greater than 10 then redoes the cycle
            System.out.println("Error: Please type a positive number between 1-10"); //Repormpts user
            System.out.println("How many rows");
            while(!scan.hasNextInt()){ //Checks again for valid input
                trash = scan.nextLine();
                System.out.println("Error: Please type in a valid input");
                System.out.println("How many rows");
            }
            rows = scan.nextInt();//Gets length again, exits as long as the interger is postive and within the intended range
            trash = scan.nextLine();//Clears buffer
        }
        for(int i = 1; i <= rows ; i++) { //Loop that perfoms action, hard to describe througnh words so wont
            for(int k = rows; k >= i; k-- ){ //Need spaces loop to get it on the other side
                System.out.print(" ");
            }
            for (int j = i ; j >= 1; j--) {
                System.out.print(j);
            }
            System.out.println();
        }
    }


}


