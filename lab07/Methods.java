/* Sam Sausville
 Homework 01
 10/24/18
 Class CSE 2 Section 210
 Methods to create a randomly generated sentence
*/

import java.util.Random;
import java.util.Scanner;

public class Methods {
    public static void main (String args[]){
    Random rand = new Random(); //Setting up imports and strings we will be using
    Scanner scan = new Scanner(System.in);
    String check = "";
    String trash = "";

    String finished = "";
    do{ //Loop to keep creating more paragraphs
        endSent(rand);
        System.out.println("Do you want another sentence?");
        while(!scan.hasNext("no") && !scan.hasNext("yes")){ //Error check loop
            trash = scan.nextLine(); //Trashs
            System.out.println("Error: Please type in an actual answer"); //Repromts the user with error message and asks again for length
            System.out.println("Do you want another sentence?");
        }
        check = scan.next();
        //trash = scan.nextLine();

    }while(!check.equalsIgnoreCase("no"));





    }

    public static void endSent(Random rand){ //Final method call
        String subject = secondSent(rand); //Calls the action sentence method
        System.out.println("That " + subject + getVerbs(rand) + "his " + getObject(rand)); //Concluding sentence
    }

    public static String intRand (Random rand, String subjectSave){
        int itRand = rand.nextInt(4); //Random number to sometimes get "it", could change depedning on wanted frequency
        String subject = ""; //Quick subject
        if(itRand >= 3){
            subject = "It "; //Changes based on random
        }else{
            subject = subjectSave;
        }
        return subject; //Returns subject
    }
    public static String secondSent(Random rand){
        String subject = subjectFirstSent(rand); //Gets subject from first dentence
        String subjectSave = subject; //saves subject in case its changed to it
        int randNum = rand.nextInt(5); //Random number for the loop
        for(int i = 0; i < randNum; i++) { //Loop to get so many action sentences
            subject = intRand(rand, subjectSave); //Gets either it or the subject
            System.out.println("This " + subject + "was " + getAdjectives(rand) + getVerbs(rand) + "to " +
                    getVerbs(rand) + getObject(rand)); //The sentence is hard to make sense without more kinds of words
            //But I dont wanna put in the effort to create another set of words, works otherwise
            subject = intRand(rand, subjectSave); //Does it again
            System.out.println(subject + "used " + getSubject(rand) + "to " + getVerbs(rand) + getObject(rand) +
                    "at the " + getAdjectives(rand) + getObject(rand)); //Same idea, sentence doesnt make sense but applies the concept wanted
            //Correctly. Just too lazy to do more words
        }
        return subjectSave; //Sends subject to the final method
    }

    public static String subjectFirstSent(Random rand){ //First sentence make
        String subject = getSubject(rand); //Gets subject
    String s = "The "  + getAdjectives(rand) + subject + //Prints out thesis sentence basically
            getVerbs(rand) +  "the " + getAdjectives(rand) + getObject(rand);
        System.out.println(s);
    return subject; //Sends sentence to next sentence to be used more
    }

    public static String getAdjectives(Random rand){ //List of adjectives that is gotten randomly
        int check = rand.nextInt(9);
        String s = "";
        switch (check){
            case 0:
                s = "Black ";
                break;
            case 1:
                s = "Brown ";
                break;
            case 2:
                s = "Stupid ";
                break;
            case 3:
                s = "Dumb ";
                break;
            case 4:
                s = "Short ";
                break;
            case 5:
                s = "Loud ";
                break;
            case 6:
                s = "Quiet ";
                break;
            case 7:
                s = "Fat ";
                break;
            case 8:
                s = "Skinny ";
                break;
            case 9:
                s = "Curious ";
        }
        return s;

    }
    public static String getSubject(Random rand){ //List of subjects gotten randomly
        int check = rand.nextInt(9);
        String s = "";
        switch (check){
            case 0:
                s = "Dog ";
                break;
            case 1:
                s = "Cat ";
                break;
            case 2:
                s = "Fox ";
                break;
            case 3:
                s = "Ewok ";
                break;
            case 4:
                s = "Rabbit ";
                break;
            case 5:
                s = "Teacher ";
                break;
            case 6:
                s = "Kid ";
                break;
            case 7:
                s = "Tiger ";
                break;
            case 8:
                s = "Cheetah ";
                break;
            case 9:
                s = "Bear ";
                break;
        }
        return s;

    }

    public static String getVerbs(Random rand){ //List of verbs gotten randomly
        int check = rand.nextInt(9);
        String s = "";
        switch (check){
            case 0:
                s = "Left ";
                break;
            case 1:
                s = "Ate ";
                break;
            case 2:
                s = "Fought ";
                break;
            case 3:
                s = "Flew ";
                break;
            case 4:
                s = "Opened ";
                break;
            case 5:
                s = "Created ";
                break;
            case 6:
                s = "Killed ";
                break;
            case 7:
                s = "Helped ";
                break;
            case 8:
                s = "Jumped ";
                break;
            case 9:
                s = "Hammered ";
                break;
        }
        return s;

    }

    public static String getObject(Random rand){ //List of objects gotten randomly
        int check = rand.nextInt(9);
        String s = "";
        switch (check){
            case 0:
                s = "Bird ";
                break;
            case 1:
                s = "Bug ";
                break;
            case 2:
                s = "Alien ";
                break;
            case 3:
                s = "Deer ";
                break;
            case 4:
                s = "Bat ";
                break;
            case 5:
                s = "Cat ";
                break;
            case 6:
                s = "Cow ";
                break;
            case 7:
                s = "Goat ";
                break;
            case 8:
                s = "Larry ";
                break;
            case 9:
                s = "Spongebob ";
                break;
        }
        return s;

    }
    }
