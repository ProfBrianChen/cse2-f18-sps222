import java.util.Arrays;
import java.util.Random;

/* Sam Sausville
 Labo 08
 11/07/18
 Class CSE 2 Section 210
 Practice with Arrays
*/
public class Array{

    public static void main (String args[]){
        Random rand = new Random(); //Set random gen
        int [] arr = new int[100]; //Create first array of 100 numbers, index 0-99
        for(int i = 0; i < arr.length; i++){ //Loop entire array length
            arr[i] = rand.nextInt(100); //fill the array with numbers between 0-99
        }
        int [] arr2 = new int[100]; //Create second array in correspondence to the first
        for(int i = 0; i < arr.length; i++){ //loop based on the length of the first array to get all of the elements to i
            arr2[arr[i]]++; // take the number from first array and correspond the index to the second array, then increment it to count
        }
        for(int i = 0; i < arr2.length; i++){ //Loop just second array to get count times
            if(arr2[i] != 0){ //If statement to make sure to not print out the ones which occur 0 times
                System.out.println(i + " occurs " + arr2[i] + " times "); //Print statement
            }
        }
    }
}