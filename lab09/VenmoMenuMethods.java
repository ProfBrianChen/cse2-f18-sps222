import java.util.Scanner;
/*
CSE 02 Section 210
Yuchen (Melody) Su,Bill Barish, Sam Sausville
Group H
Venmo Menu Methods
*/
public class VenmoMenuMethods{
    public static void main(String [] args){
        Scanner input = new Scanner(System.in);
        String [] friends = {"Sam", "Melody", "Bill", "", ""};
        double [] moneySent = new double[5];
        double [] moneyRec = new double[5];


        int choice;
        double balance = 100;
        double money = 0;
        String friend;
        do{
            printMenu();
            choice = getInt();
            switch (choice){
                case 1: getBalance(balance);
                    balance = sendMoney(balance, friends, moneySent);
                    break;
                case 2: balance = requestMoney(balance, friends, moneyRec);
                    break;
                case 3: getBalance(balance);
                    break;
                case 4: System.out.println("Goodbye");
                    break;
                case 5: addFriend(friends);
                    break;
                case 6: printFriendsReport(friends, moneySent, moneyRec);
                    break;
                default: System.out.println("you entered an invalid value -- try again");
                    break;
            }
        }while(choice != 4);
    }


    //print Venmo Main Menu
    public static void printMenu(){
        System.out.println("Venmo Main Menu");
        System.out.println("1. Send Money");
        System.out.println("2. Request Money");
        System.out.println("3. Check Balance");
        System.out.println("4. Quit");
        System.out.println("5. Add Friend");
        System.out.println("6. Show Report");
    }

    //get a number and check for an integer
    public static int getInt(){
        Scanner input = new Scanner(System.in);
        while(input.hasNextInt() == false){
            System.out.println("You entered and invalid value -- try again");
            input.nextLine();//clear buffer
        }
        return input.nextInt();
    }
    public static void getBalance(double balance){
        System.out.println("You have $" + balance + " in your account" );
    }

    //get a number and check for a double
    public static double getDouble(){
        Scanner input = new Scanner(System.in);
        while(input.hasNextDouble() == false){
            System.out.println("You entered and invalid value -- try again");
            input.nextLine(); //clear buffer
        }
        return input.nextDouble();
    }

    //driver method for sendMondy
    public static double sendMoney(double balance, String [] friends, double [] sent){
        double money = 0;
        System.out.println("How much money do you want to send?");
        money = getDouble();//get and check user input for a double
        balance = checkMoney(money,balance, friends, sent); //check valid amount no negative and if enough money is in balance to send money
        return balance;
    }

    //method prints friend and money sent to friend
    public static void getFriend(double money){
        Scanner input = new Scanner(System.in);
        System.out.println("Who do you want to send your money to?");
        String friend = input.nextLine();
        System.out.println("You have successfully sent "+ money +" to "+ friend);
    }

    /*method that makes sure money entered is greater than 0,
     * there are sufficient funds in balance, if sufficient funds the balance is changed
     * and returned. if funds are not sufficient the user is taken back to the main menu
     */
    public static double checkMoney(double money, double balance, String [] friends, double[] sent){
        while (money <= 0){
            System.out.println("Invalid amount entered");
            money = getDouble();
        }
        System.out.println("Who do you want to send money too");
        int check = getFriendRequest(money, friends);
        if (money <= balance){
            if( check != -1){
                addArraySent(sent, money, check);
                balance -= money;
                System.out.println("You are sending $" + money + " from "+ friends[check]);
                System.out.println("Once confirmed, your new balance will be $" + balance);
            }else{
                System.out.println("Not in your friends list");
            }
        }
        else{
            System.out.println("You do not have sufficient funds for this transaction.");
        }
        return balance;
    }

    //driver method for requestMonday
    public static double requestMoney(double balance, String [] friends, double[] rec){
        System.out.println("How much money do you want to request?");
        double money = getDouble();
        balance = checkRequestMoney(money,balance,friends, rec);
        return balance;

    }

    //gets friends information for the Request menu item and prints the amount requested from friend
    public static int getFriendRequest(double money, String [] friends){
        Scanner input = new Scanner(System.in);
        String friend = input.nextLine();
        int track = -1;
        for(int i = 0; i < friends.length; i++){
            if(friends[i].equalsIgnoreCase(friend)){
                track = i;
            }
        }
        return track;
    }

    //method makes sure money is greater than 0 and adds requested amount to balance and returns new balance
    public static double checkRequestMoney(double money, double balance, String [] friends, double [] rec){
        while (money <= 0){
            System.out.println("Invalid amount entered");
            money = getDouble();
        }
        System.out.println("Who do you want to request money from?");
        int check = getFriendRequest(money, friends);
        if( check != -1){
            addArrayRec(rec, money, check);
            balance += money;
            System.out.println("You are requesting $" + money + " from "+ friends[check]);
            System.out.println("Once confirmed, your new balance will be $" + balance);
        }else{
            System.out.println("Not in your friends list");
        }
        return balance;
    }
//simply functions that add tbe amounts to the arrays of sent or received
    public static void addArraySent(double[] sent, double money, int index){
        sent[index] += money;
    }
    public static void addArrayRec(double[] rec, double money, int index){
        rec[index] += money;
    }

    //Adds frends to the array list, if its full doesnt add a friend. Would add a remove friend feature in the future
    //or use an ArrayList ;)
    public static void addFriend (String [] friends){
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the desired friends Name");
        String friend = scan.nextLine();
        boolean b = false;
        for (int i = 0; i < friends.length; i++){
            if(friends[i].isEmpty()){
                friends[i] = friend;
                b = true;
                break;
            }
        }
        if (b == false){
            System.out.println("Not enough space on your friends list");
        }
    }

    //Prints out the arrays at the index required
    public static void printFriendsReport(String [] friends, double [] sent, double [] rec){
        for(int i = 0; i < friends.length; i++){
            System.out.println("Friends: " + friends[i]);
            System.out.println("Money Sent overtime: " + sent[i]);
            System.out.println("Money recieved overtime: " + rec[i]);
        }
    }

}
